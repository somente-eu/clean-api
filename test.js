let should = require('should')

describe('Initializing tests....', function () {
    it('should init without error...', function () {
        (0).should.be.equal(0)
    })
})

describe('Finance', function () {
    it('should create invoice for the month if it not exists...', function () {
        let repo = require('./src/database/finance')
            (repo.getCurrentInvoice).should.be.notEqual(0)
    })
})