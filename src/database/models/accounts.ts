import Database from "../../services/database";
import Sequelize, { Model } from 'sequelize';

export interface IAccountRow {
    id: number;
    email: string;
    password: string;
    name: string;
    role: number;
    profile_pic: string;
    createdAt: string;
    updatedAt: string;
}

export default class AccountsModel extends Model {
    static init(sequelize: any){
        super.init({
            email: Sequelize.STRING,
            password: Sequelize.STRING,
            name: Sequelize.STRING,
            role: Sequelize.INTEGER,
            profile_pic: Sequelize.TEXT,
        }, {
            sequelize,
            freezeTableName: true,
            tableName: 'accounts'
        });

        return this;
    }

    static associate(models: any) {
    }

    static async findById(id: number): Promise<any> {
        let sql = await this.findOne({
            where: {
                id
            }
        });
        let data = Database.getResults<IAccountRow>(sql);

        if (!data)
            return false;
        return data;
    }

    static async findByEmail(email: any): Promise<any> {
        let sql = await this.findOne({
            where: {
                email
            }
        });
        let data = Database.getResults<IAccountRow>(sql);

        if (!data)
            return false;
        return data;
    }

    static async findAllUsers(): Promise<any> {
        let sql = await this.findAll();
        let data = Database.getResults<IAccountRow>(sql);

        if (!data)
            return false;
        return data;
    }
}