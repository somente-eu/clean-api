import { setExternalRoute } from './';
import AccountsController from '../controllers/CAccount';
import ControllerConstructor from '../controllers';

const fn = (fn: any) => {
    return (req: any, res: any) => {
        ControllerConstructor.RouteConstructor(req, res, fn);
    };
};

export function setRoutesUp(app: any) {
    setExternalRoute(app, 'post', '/login', fn(AccountsController.login), false);
}