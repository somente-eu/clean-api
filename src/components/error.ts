interface IErrorModel {
  code: number;
  message: string;
}

export default class AppError implements Error {
  static errorModels = {
    TEST: 1000,
    INVALID_LOGIN: 1001,
    VALIDATE_FAIL: 1002,
  };

  name: string = "";
  message: string;
  code: number;

  constructor(errcode: number) {
    let msg = "UNKNOWN";

    let keys = Object.keys(AppError.errorModels);
    keys.map((k) => {
      if ((AppError.errorModels as any)[k] === errcode)
        msg = k;
    });

    this.code = errcode;
    this.message = msg;
  }
}