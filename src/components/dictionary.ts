export default class Dictionary<T> {
    private count: number = 0;
    // private maxSize: number = 0;

    private objs: {
        [key: number]: T;
    } = {

        };

    get size() {
        return this.count;
    }

    add(id: number, element: T) {
        if (!this.objs[id]) {
            this.objs[id] = element;
            this.count++;
        }
    }

    remove(id: number) {
        delete this.objs[id];
        this.count--;
    }

    get(id: number) {
        return this.objs[id];
    }

    contains(id: number): boolean {
        try {
            let pObj = this.objs[id];

            if (pObj)
                if (pObj !== null)
                    return true;
            return false;
        } catch {
            return false;
        }
    }

    map(cb: (element: T, index: number, originalIndex: string | number) => any): any {
        let _i = 0;
        for (let index in this.objs) {
            let element = this.objs[index];
            if (cb)
                cb(element, _i, index);
            _i++;
        }
    }

    toArray(): T[] {
        let arr: T[] = [];
        this.map((e) => {
            arr.push(e);
        });

        return arr;
    }

    filter(validatorFunction: (element: T) => boolean, idKey = 'id') {
        let filtered = new Dictionary<T>();
        this.map((element: any) => {
            if (validatorFunction(element))
                filtered.add(element[idKey], element);
        });

        return filtered;
    }

}

export class SDictionary<T> {
    private count: number = 0;
    // private maxSize: number = 0;

    private objs: {
        [key: string]: T;
    } = {

        };

    get size() {
        return this.count;
    }

    add(id: string, element: T) {
        if (!this.objs[id]) {
            this.objs[id] = element;
            this.count++;
        }
    }

    remove(id: string) {
        delete this.objs[id];
        this.count--;
    }

    get(id: string) {
        return this.objs[id];
    }

    contains(id: string): boolean {
        try {
            let pObj = this.objs[id];

            if (pObj)
                if (pObj !== null)
                    return true;
            return false;
        } catch {
            return false;
        }
    }

    map(cb: (element: T, index: number, originalIndex: string | number) => any): any {
        let _i = 0;
        for (let index in this.objs) {
            let element = this.objs[index];
            if (cb)
                cb(element, _i, index);
            _i++;
        }
    }

    toArray(): T[] {
        let arr: T[] = [];
        this.map((e) => {
            arr.push(e);
        });

        return arr;
    }

    filter(validatorFunction: (element: T) => boolean, idKey = 'id') {
        let filtered = new Dictionary<T>();
        this.map((element: any) => {
            if (validatorFunction(element))
                filtered.add(element[idKey], element);
        });

        return filtered;
    }

}