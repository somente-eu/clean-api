import { logInfo, logRequest } from './services/logger';
import * as moment from 'moment';

// Routes
import * as ImgUploader from './routes/upload/img';
import * as FileUploader from './routes/upload/file';
import route from './routes/';
import Database from './services/database';
import Utils from './helpers/utils';

const pkg = require('../package.json');

// Libs
const Sentry = require('@sentry/node');
const cors = require('cors');
const bodyParser = require('body-parser');

export default class App {
    static fullyLoaded = false;

    static express = require('express');
    static instance = App.express();

    configureApp(app: any) {
        app.use(Sentry.Handlers.requestHandler());
        app.use(bodyParser.json({
            limit: 4194304
        }));
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cors());

        app.use(logRequest);

        ImgUploader.UploadImg(app);
        FileUploader.UploadFile(app);

        route(app);

        app.use(Sentry.Handlers.errorHandler());
        app.use(function onError(err: any, req: any, res: any, next: any) {
            res.statusCode = 500;
            res.end(res.sentry + '\n');
        });
    }

    prepareDatabase() {
        Database.create();
    }

    start() {
        logInfo(`Currently Server Version: v${pkg.version}`);

        Utils.overrideDefaults();
        this.prepareDatabase();
        this.configureApp(App.instance);

        App.instance.listen(process.env.HTTP_PORT);
        logInfo(`API ready. Listening on port ${process.env.HTTP_PORT}`);

        let startMsg = `API Started at ${moment().format()}`;

        logInfo(startMsg);
        Sentry.captureMessage(startMsg);
    }
}