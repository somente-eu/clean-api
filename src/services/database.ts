import * as Settings from '../settings';
import AccountsModel from '../database/models/accounts';
import { logInfo } from './logger';
const Sequelize = require('sequelize');

class MySQLDatabase {
    connection: any = null;
    models: any = {};

    create() {
        this.connection =  new Sequelize(process.env.DB_BASE, process.env.DB_USER, process.env.DB_PASS, {
            host: process.env.DB_HOST,
            dialect: 'mysql'
        });

        this.connection.authenticate()
        .then(() => {
            Settings.logInfo('Connected to Database!');
            this.loadModels();
        })
        .catch((err: any) => {
            Settings.logError(`Error connecting to DB: ${err.message}. Retrying in 10 seconds...`);

            setTimeout(() => {
                this.create();
            }, 10000);
            return;
        });

        return this;
    }

    loadModels(){
        this.models.accounts = AccountsModel;

        this.connection.sync().then(() => {
            // SYNC DATABASE MODELS
            logInfo('Associating Models...');
            this.models.accounts.associate(this.models);
        });
    }

    getResults<T = any>(sql: any){
        let results: T[] = [];

        if (Array.isArray(sql)){
            if (sql){
                sql.map((row: any) => {
                    results.push(Object.assign({}, row.dataValues));
                });
            }
        } else {
            if (sql)
                results.push(Object.assign({}, sql.dataValues));
        }

        return results as T[];
    }

    getFirstResult<T = any>(sql: any){
        let results = this.getResults<T>(sql);
        if (Array.isArray(results))
            return results[0];
        return Object.assign({}, results);
    }
}

const Database = new MySQLDatabase();
export default Database;