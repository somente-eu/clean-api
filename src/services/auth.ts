import * as Settings from '../settings';
import * as tokenService from './token';
import AccountsModel from '../database/models/accounts';

export async function login(email: string, password: string, res: any, resolve: any, reject: any): Promise<string> {
    let data: any = await AccountsModel.findByEmail(email);
    if (!data || data.length < 1) { return res.json({ error: 'INVALID_LOGIN' }); }
    let userdata: any = data;
    if (!userdata || password != userdata.password) { return res.json({ error: 'INVALID_LOGIN' }); }

    let token = await generateToken(userdata.id, userdata.role);

    let user = {
        id: userdata.id,
        email: userdata.email,
        name: userdata.name,
        role: userdata.role,
        token,
        profile_pic: userdata.profile_pic,
        address: userdata.address,
        number: userdata.number,
        neighborhood: userdata.neighborhood,
        city: userdata.city,
        state: userdata.state,
        country: userdata.country,
        postal_code: userdata.postal_code,
        phone: userdata.phone
    };

    return resolve(user);

}

export async function logout(resolve: any, reject: any): Promise<string> {
    return resolve(true);
}

async function generateToken(user: any, role: any) {
    return tokenService.userToken(user, role);
}

export async function authorizeAdminRole(req: any, res: any, next: any) {
    if (req.role >= Settings.USER_ROLE.Admin) {
        next();
    } else {
        return res.status(403).send({ auth: false, message: 'You have no access to this.' });
    }
}

export async function authorizeModeratorRole(req: any, res: any, next: any) {
    if (req.role >= Settings.USER_ROLE.Moderator) {
        next();
    } else {
        return res.status(403).send({ auth: false, message: 'You have no access to this.' });
    }
}

export async function authorizeUserRole(req: any, res: any, next: any) {
    if (req.role >= Settings.USER_ROLE.User) {
        next();
    } else {
        return res.status(403).send({ auth: false, message: 'You have no access to this.' });
    }
}