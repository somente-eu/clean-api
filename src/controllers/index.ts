import { Request, Response } from 'express';
import { logError } from '../services/logger';

export default class ControllerConstructor {
  static async RouteConstructor(req: Request, res: Response, next: any) {
    try {
      let result = await next(req, res);

      if (!res.finished) {
        res.json({
          success: true,
          result
        });
      }
    } catch (err) {
      logError(err);

      if (!res.finished) {
        res.json({
          success: false,
          code: err.code,
          message: err.message,
        });
      }
    }
  }
}