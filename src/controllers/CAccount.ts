import { Request, Response } from 'express';
import * as Yup from 'yup';
import { getValidatedObjectFromSchema, validateDataSchema } from '../helpers/validators';

export default class AccountsController {
    static async login(req: Request, res: Response) {
        let { body } = req;

        const schema = Yup.object().shape({
            username: Yup.string().required('NO_USERNAME').min(4, 'INVALID_USERNAME'),
            password: Yup.string().required('NO_PASSWORD').min(4, 'INVALID_PASSWORD'),
        });

        if (await validateDataSchema(body, schema, res)){
            let obj = await getValidatedObjectFromSchema(schema, body);
            console.log(obj);
        }
    }
}