declare namespace Express {
  interface Request {
    /* ID number of the user assumed to the request. */
    userId: number

    /* Role number of the user assumed to the request. */
    role: number

    /* Token of the user sent with the request. */
    token: string
  }
}